<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Book;
use yii\data\ActiveDataProvider;
use app\models\Author;
use app\models\BookFilterForm;
use app\models\NewBookForm;
use app\models\RegistrationForm;
use app\models\LoginForm;
use yii\data\Pagination;
use app\models\AddAuthorForm;
use app\models\AddCategoryForm;


class BookController extends Controller{ 
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    private $books;
    private $bookPages;
    private $addBookModel;
    private $selectedCategory;
    private $addCategoryModel;
    private $addAuthorModel;
            
    function __construct($id, $module){
        parent::__construct($id, $module);
     }
     
    public function Initialization(){
        $_SESSION["category"] = [];
        $model = Book::find()
                ->with('authors')
                ->with('categories');
        $countQuery = clone $model;
        $this->bookPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>3]);
        $this->books = $model->offset($this->bookPages->offset)
                 ->limit($this->bookPages->limit)
                 ->all();
        $this->addBookModel = new NewBookForm();
        $this->selectedCategory = "category";
        $this->addCategoryModel = new AddCategoryForm();
        $this->addAuthorModel = new AddAuthorForm();    
    }
    
    public function actionIndex(){
        return $this->render('//site/index',[
            "books"=>$this->books,
            "bookPages" => $this->bookPages,
            "addBookModel" => $this->addBookModel,
            "selectedCategory"=> $this->selectedCategory,
            "addCategoryModel" => $this->addCategoryModel,
            "addAuthorModel"=> $this->addAuthorModel
            ]);
    } 
     
    public function actionAddcategory(){
       $id = Yii::$app->getRequest()->getQueryParam('id');
       
    }
    
    public function actionAddauthor(){
       $id = Yii::$app->getRequest()->getQueryParam('id');
       
    }
  
}
