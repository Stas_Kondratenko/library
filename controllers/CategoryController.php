<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Book;
use yii\data\ActiveDataProvider;
use app\models\Author;
use app\models\BookFilterForm;
use app\models\NewBookForm;
use app\models\RegistrationForm;
use app\models\LoginForm;
use yii\data\Pagination;
use app\models\SelectCategory;
use app\models\AddCategoryForm;
use app\models\Category;
use app\models\AddAuthorForm;


class CategoryController extends Controller{ 
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    private $books;
    private $bookPages;
    private $addBookModel;
    private $selectedCategory;
    private $addCategoryModel;
    private $addAuthorModel;
            
    function __construct($id, $module){
        parent::__construct($id, $module);
        $this->Initialization();   
     }
    
    public function actionIndex(){
        return $this->render('//site/index',[
            "books"=>$this->books,
            "bookPages" => $this->bookPages,
            "addBookModel" => $this->addBookModel,
            "selectedCategory"=> $this->selectedCategory,
            "addCategoryModel" => $this->addCategoryModel,
            "addAuthorModel"=> $this->addAuthorModel
            ]);
    } 
    
    public function Initialization(){
        $model = Book::find()
                ->with('authors')
                ->with('categories');
        $countQuery = clone $model;
        $this->bookPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>3]);
        $this->books = $model->offset($this->bookPages->offset)
                 ->limit($this->bookPages->limit)
                 ->all();
        $this->addBookModel = new NewBookForm();
        $this->selectedCategory = "category";
        $this->addCategoryModel = new AddCategoryForm();
        $this->addAuthorModel = new AddAuthorForm();    
    }

    public function actionAdd(){
          if ($this->addCategoryModel->load(Yii::$app->request->post())){
               if($this->addCategoryModel->validate()){
                  $category = new Category(); 
                  $category->name = $this->addCategoryModel->name;
                  try{
                     $category->save();
                     $this->Initialization();
                     return $this->refresh();
                  }
                   catch (\yii\db\Exception $ex){
                       $this->addCategoryModel->addError("name","That category is already exist.");
                       return $this->actionIndex();
                   }
                   
               }
               return $this->actionIndex();
          }
          return $this->actionIndex();
    }
    
    public function actionSelect(){
         $id = Yii::$app->getRequest()->getQueryParam('id');
         $model = Book::find()
                ->where(["category.category_id"=>$id])
                ->innerJoinWith('categories')
                ->with('authors')
                ->with('categories');
        $countQuery = clone $model;
        $this->bookPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>6]);
        $this->books = $model->offset($this->bookPages->offset)
                 ->limit($this->bookPages->limit)
                 ->all();
        return $this->render('//site/index',[
                    "books"=>$this->books,
                    "bookPages" => $this->bookPages,
                    "addBookModel" => $this->addBookModel,
                    "selectedCategory"=> $this->selectedCategory,
                    "addCategoryModel" => $this->addCategoryModel
                    ]);
    }
    
   
  
}
