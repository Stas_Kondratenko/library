<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Book;
use yii\data\ActiveDataProvider;
use app\models\Author;
use app\models\BookFilterForm;
use app\models\NewBookForm;
use app\models\RegistrationForm;
use app\models\LoginForm;
use yii\data\Pagination;
use app\models\Category;
use app\models\AddCategoryForm;
use app\models\AddAuthorForm;


class SiteController extends Controller{ 
    
    private $books;
    private $bookPages;
    private $addBookModel;
    private $selectedCategory;
    private $addCategoryModel;
    private $addAuthorModel;


    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    function __construct($id, $module){
        parent::__construct($id, $module);
        $model = Book::find()
                ->with('authors')
                ->with('categories');
        $countQuery = clone $model;
        $this->bookPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>9]);
        $this->books = $model->offset($this->bookPages->offset)
                 ->limit($this->bookPages->limit)
                 ->all();
        $this->addBookModel = new NewBookForm();
        $this->selectedCategory = "book";
        $this->addCategoryModel = new AddCategoryForm();
        $this->addAuthorModel = new AddAuthorForm(); 
     }

    public function actionIndex(){
        return $this->render('index',[
            "books"=>$this->books,
            "bookPages" => $this->bookPages,
            "addBookModel" => $this->addBookModel,
            "selectedCategory"=> $this->selectedCategory,
            "addCategoryModel" => $this->addCategoryModel,
            "addAuthorModel"=> $this->addAuthorModel
            ]);
    }

    public function actionAbout() {
        return $this->render('about');
    }
    
    public function actionLogin(){
        return $this->render('login',['model'=>new LoginForm()]);
     }
     
    public function actionRegistration(){
        return $this->render('registration',['model'=> new RegistrationForm()]);
     }  
}
