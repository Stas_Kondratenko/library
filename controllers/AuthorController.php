<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Book;
use app\models\Author;
use app\models\BookFilterForm;
use app\models\NewBookForm;
use app\models\RegistrationForm;
use app\models\LoginForm;
use yii\data\Pagination;
use app\models\AddCategoryForm;
use app\models\AddAuthorForm;


class AuthorController extends Controller{ 
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    private $books;
    private $bookPages;
    private $addBookModel;
    private $selectedCategory;
    private $addCategoryModel;
    private $addAuthorModel;
            
    function __construct($id, $module){
        parent::__construct($id, $module);
        $this->Initialization();
     }
     
    public function Initialization(){
        $model = Book::find()
                ->with('authors')
                ->with('categories');
        $countQuery = clone $model;
        $this->bookPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>3]);
        $this->books = $model->offset($this->bookPages->offset)
                 ->limit($this->bookPages->limit)
                 ->all();
       $this->addBookModel = new NewBookForm();
       $this->selectedCategory = "author";
       $this->addCategoryModel = new AddCategoryForm();
       $this->addAuthorModel = new AddAuthorForm();     
     }

    public function actionIndex(){
        return $this->render('//site/index',[
            "books"=>$this->books,
            "bookPages" => $this->bookPages,
            "addBookModel" => $this->addBookModel,
            "selectedCategory"=> $this->selectedCategory,
            "addCategoryModel" => $this->addCategoryModel,
            "addAuthorModel"=> $this->addAuthorModel
            ]);
    } 
     
    public function actionSelect(){
         $id = Yii::$app->getRequest()->getQueryParam('id');
         $model = Book::find()
                ->where(["author.author_id"=>$id])
                ->innerJoinWith('authors')
                ->with('authors')
                ->with('categories');
        $countQuery = clone $model;
        $this->bookPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>6]);
        $this->books = $model->offset($this->bookPages->offset)
                 ->limit($this->bookPages->limit)
                 ->all();
        return $this->render('//site/index',[
                    "books"=>$this->books,
                    "bookPages" => $this->bookPages,
                    "addBookModel" => $this->addBookModel,
                    "selectedCategory"=> $this->selectedCategory
                    ]);
    }
    
    public function actionAdd(){
          if ($this->addAuthorModel->load(Yii::$app->request->post())){
               if($this->addAuthorModel->validate()){
                  $author = new Author(); 
                  $author->first_name = $this->addAuthorModel->first_name;
                  $author->second_name = $this->addAuthorModel->second_name;
                  try{
                     $author->save(); 
                     $this->Initialization();
                     return $this->refresh();
                  }
                   catch (\yii\db\Exception $ex){
                       $this->addAuthorModel->addError("name","This author is already exist.");
                       return $this->actionIndex();
                   }
                   
               }
               return $this->actionIndex();
          }
          return $this->actionIndex();
    }
  
}
