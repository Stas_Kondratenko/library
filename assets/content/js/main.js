jQuery(document).ready(function ($) { 
    $("#add-book-btn").click(function () {
        $("#add-book-window").modal();
    });
    
    $("#add-category-btn").click(function () {
        $("#add-category-window").modal();
    });
    
    $("#add-author-btn").click(function () {
        $("#add-author-window").modal();
    });
    
});