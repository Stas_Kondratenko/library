<?php
namespace app\models;

use yii\base\Model;

class AddCategoryForm extends Model{
    
    public $name;
    
    public function rules()  {
           return [  
            ['name', 'filter', 'filter' => 'trim'],  
            ['name', 'required'],
            ['name', 'string', 'min' => 5, 'max' => 40],
        ];
    }
}