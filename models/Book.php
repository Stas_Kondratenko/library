<?php
namespace app\models;
use yii\db\ActiveRecord;
use app\models\Author;
use app\models\Category;

class Book extends ActiveRecord {
    private $book_id;
    private $name;
    private $cover;
    private $body;

    public static function tableName(){
        return 'book';
    }
    
    public function getAuthors(){  
       return $this->hasMany(Author::className(), ['author_id' => 'author_id'])->viaTable('book_author', ['book_id' => 'book_id']);
    }
    
    public function getCategories(){  
       return $this->hasMany(Category::className(), ['category_id' => 'category_id'])->viaTable('book_category', ['book_id' => 'book_id']);
    }
    
    
}  