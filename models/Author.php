<?php
namespace app\models;
use yii\db\ActiveRecord;

class Author extends ActiveRecord 
{
    private $id;
    private $first_name;
    private $last_name;
  
    public static function tableName(){
        return 'author';
    }
}  