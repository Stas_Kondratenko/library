<?php
namespace app\models;

use yii\base\Model;

class AddAuthorForm extends Model{
    
    public $first_name;
    public $second_name;
    
    public function rules()  {
           return [  
            ['first_name', 'filter', 'filter' => 'trim'],  
            ['first_name', 'required'],
            ['first_name', 'string', 'min' => 5, 'max' => 40],
               
            ['second_name', 'filter', 'filter' => 'trim'],  
            ['second_name', 'required'],
            ['second_name', 'string', 'min' => 5, 'max' => 40],
        ];
    }
}