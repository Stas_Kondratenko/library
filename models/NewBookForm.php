<?php
namespace app\models;

use yii\base\Model;

class NewBookForm extends Model{
    public $name;
    public $authors;
    public $categories;
    public $cover;
    public $body;
    
    public function rules()  {
           return [  
            ['name', 'filter', 'filter' => 'trim'],  
            ['name', 'required'],
            ['name', 'string', 'min' => 5, 'max' => 40],

            ['authors', 'filter', 'filter' => 'trim'],
            ['authors', 'required'],
            ['authors', 'string', 'min' => 5, 'max' => 40],
               
            ['categories', 'filter', 'filter' => 'trim'],
            ['categories', 'required'],
            ['categories', 'string', 'min' => 5, 'max' => 40],
     
            ['cover', 'image', 'extensions' => ['png', 'jpg', 'gif'], 
             'minWidth' => 100, 'maxWidth' => 1920,
             'minHeight' => 100, 'maxHeight' => 1080,],
               
            ['body', 'file', 'extensions' => ['txt', 'docx', 'pdf']]
             

        ];
    }
}