<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\LoginForm;
use app\models\RegistrationForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link href="assets/content/images/logo.png" rel="icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
   <?php
    NavBar::begin([
        'brandLabel' => Html::img('assets/content/images/logo.png'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top navbar-default',
            'id' => 'main-menu'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left navbar-default'],
        'items' => [
            ['label' => 'Library', 'url' => ['/site/index']],
        ],
    ]);
    $menuItems = [];
    if(Yii::$app->user->isGuest){
        $menuItems[] = ['label' => 'Login','url' => ['/site/login']];
        $menuItems[] = ['label' => 'Registration','url' => ['/site/registration']]; 
    }
    else{
        $menuItems[] = [
            'label' => 'Add',
            'items' => [
                 ['label' => 'Add book',
                  "options" =>["id"=>"add-book-btn"],
                  'url' => '#'   ],
                ['label' => 'Add category',
                  "options" =>["id"=>"add-category-btn"],
                  'url' => '#'   ],
                ['label' => 'Add author',
                  "options" =>["id"=>"add-author-btn"],
                  'url' => '#'   ],
            ],
            ];
        $menuItems[] = ['label' => 'LogOut', 'url' => ['/authentication/logout']];  
    }
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems
    ]);  
    
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
