<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
?>

<div class="modal fade" id="add-author-window">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Add author</h4>
                  </div>
                  <div class="modal-body">   
                   <?php Pjax::begin(); ?>
                    <?php $authorForm = ActiveForm::begin([
                                 'id' => 'add-author-form',
                                 'method' => 'post',
                                 'action' => ['author/add'],
                                 'options' => ['onsubmit' => 'return false','data-pjax' => true],
                                 'enableClientValidation' => true,
                                  ]); ?>

                      <div class="form-group">
                           <?= $authorForm->field($addAuthorModel, 'first_name') ?>
                      </div>
                      <div class="form-group">
                           <?= $authorForm->field($addAuthorModel, 'second_name') ?>
                      </div>
                   
                 <div class="modal-footer">
                     <button type="submit" class="btn btn-primary">Add</button>
                      <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
                  <?php ActiveForm::end(); ?>  
              <?php Pjax::end(); ?>
              </div>
           </div>
    </div> 
</div>
