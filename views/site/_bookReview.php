<?php 
use yii\helpers\Html;

foreach ($books as $book){
    $authors = "";
    $categories = "";
    foreach ($book->authors as $author){
       $authors .=" ". Html::a($author->first_name ." ". $author->second_name,['author/select', 'id' =>$author->author_id] );
    }
    foreach ($book->categories as $category){
       $categories .=" ". Html::a($category->name,['category/select', 'id' =>$category->category_id] );
    }
    
 echo 
 "<div class='col-md-4'>
    <div class='thumbnail'>
     ".Html::img('@web/uploads/'.$book->cover,["class" => "cover-img"] )."
        <div class='caption'>
            <h4><a href='#'>". $book->name."</a></h4>
            <p> By ".$authors."</p>
            <p> Categories: ".$categories."</p>
        ".Html::a("Download book",'@web/uploads/'.$book->body,['data-pjax'=>0])."
        </div>
    </div>
</div>";  
}  
?>

