<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\bootstrap\Nav;
$this->title = 'Library';
$selectedCategory;
?>
  <?php Pjax::begin(['timeout' => 5000]); ?>
<div class="site-index">
    <div class="col-md-3"> 
            <?php
            $menuItems = [];
            if($selectedCategory == "book"){
                   $menuItems[] = ['label' => 'Books', 'url' => ['/site/index'],"options" =>["class"=>"active"]];
                   $menuItems[] = ['label' => 'Categories', 'url' => ['/category/index']];
                   $menuItems[] = ['label' => 'Authors', 'url' => ['/author/index']];
            }
            else if($selectedCategory == "category"){
                   $menuItems[] = ['label' => 'Books', 'url' => ['/site/index']];
                   $menuItems[] = ['label' => 'Categories', 'url' => ['/category/index'],"options" =>["class"=>"active"]];
                   $menuItems[] = ['label' => 'Authors', 'url' => ['/author/index']];
            }
            else if($selectedCategory == "author"){
                   $menuItems[] = ['label' => 'Books', 'url' => ['/site/index']];
                   $menuItems[] = ['label' => 'Categories', 'url' => ['/category/index']];
                   $menuItems[] = ['label' => 'Authors', 'url' => ['/author/index'],"options" =>["class"=>"active"]];
            }
 
            echo Nav::widget([
            'options' => ['class' => 'nav nav-pills nav-stacked'],
            'items' => $menuItems
                ]);
             ?>

    </div> 
    
    <div class="col-md-9">      
                <?php 
                if($selectedCategory == "book"){
                    echo $this->render("_bookReview",["books" => $books]);
                }
                else if($selectedCategory == "category"){
                    echo $this->render("_categoryReview",["books" => $books]);
                }
                else if($selectedCategory == "author"){
                    echo $this->render("_authorReview",["books" => $books]);
                }
                ?>
    </div>  
</div>
 <footer class="footer">
    <div class="container text-center navbar-fixed-bottom">
          <?php   echo LinkPager::widget([
                 'pagination' => $bookPages,
            ]); 
          ?>
    </div>
</footer>
 <?php Pjax::end(); ?>

 <?php echo $this->render("//book/add",["addBookModel" => $addBookModel]); ?>
 <?php echo $this->render("//category/add",["addCategoryModel" => $addCategoryModel]);?>
 <?php echo $this->render("//author/add",["addAuthorModel" => $addAuthorModel]);

 
