<?php 
use app\models\Author;
use yii\helpers\Html;

$authors = Author::find()->andWhere(['>','author_id',0])->all();
?>
<div class="col-md-4">
    <div class="list-group">
        <?php
          foreach ($authors as $author){
              if(isset($_GET['id'])){
                  if($author->author_id == $_GET['id']){
                        echo Html::a($author->first_name. " ".$author->second_name,['author/select', 'id' =>$author->author_id],["class"=>"list-group-item active"] );   
                  }
                  else{
                        echo Html::a($author->first_name. " ".$author->second_name,['author/select', 'id' =>$author->author_id],["class"=>"list-group-item"] );  
                  } 
              }
              else{
                  echo Html::a($author->first_name. " ".$author->second_name,['author/select', 'id' =>$author->author_id],["class"=>"list-group-item"] );
              }   
          }
        ?>
    </div> 
</div>
<div class="col-md-8">
        <?php  foreach ($books as $book){
          $authors = "";
          $categories = "";
          foreach ($book->authors as $author){
             $authors .=" ". Html::a($author->first_name ." ". $author->second_name,['author/select', 'id' =>$author->author_id] );
          }
          foreach ($book->categories as $category){
             $categories .=" ". Html::a($category->name,['category/select', 'id' =>$category->category_id] );
          }

                    echo 
                    "<div class='col-md-6'>
                       <div class='thumbnail'>
                        ".Html::img('@web/uploads/'.$book->cover,["class" => "cover-img"] )."
                           <div class='caption'>
                               <h4><a href='#'>". $book->name."</a></h4>
                               <p> By ".$authors."</p>
                               <p> Categories: ".$categories."</p>
                           ".Html::a("Download book",'@web/uploads/'.$book->body,['data-pjax'=>0])."
                           </div>
                       </div>
                   </div>";  
      }  
      ?>   
</div>