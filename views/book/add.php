<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\Author;
use app\models\SelectCategory;
use yii\widgets\Pjax;
use yii\helpers\Html;

$categories = Category::find()->andWhere(['>','category_id',0])->all();
$authors = Author::find()->andWhere(['>','author_id',0])->all();
?>

<div class="modal fade" id="add-book-window">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Add book</h4>
                  </div>
                  <div class="modal-body">
                     <?php Pjax::begin(['timeout' => 5000]);  ?>                            
                        <?php $form = ActiveForm::begin([
                                     'id' => 'add-book-form',
                                     'method' => 'post',
                                     'action' => ['book/add'],
                                     'options' => ['enctype'=>'multipart/form-data',
                                     'onsubmit' => 'return false','data-pjax' => true],
                                     'enableClientValidation' => true,
                                      ]); ?>
                      
                          <p>Categories:  </p>  
                          <?php 
                          if (isset($session['category'])){print_r($session['category']);}
                          ?>
                          
                          
                          <p>Authors:   </p> 

                          <div class="form-group">
                               <?= $form->field($addBookModel, 'name')->label("Name") ?>
                          </div>
                          <div class="form-group">
                               <?= $form->field($addBookModel, 'cover')->fileInput(['accept' => 'image/gif,image/jpeg,image/png'])->label("Book cover") ?>
                          </div>
                           <div class="form-group">
                               <?= $form->field($addBookModel, 'body')->fileInput(['accept' => 'application/pdf,text/plain,application/msword'])->label("Book file") ?>
                          </div>

                          <?php ActiveForm::end(); ?>   
                      
                      
                          <p>Add book authors : </p>
                          <?php 
                            foreach ($authors as $author){
                              echo " /". Html::a($author->first_name ." ". $author->second_name,['book/addauthor', 'id' =>$author->author_id] );
                            }
                          ?>
                          <hr>
                          
                          <p>Add book categories : </p>
                          <?php
                            foreach ($categories as $category){
                              echo " /". Html::a($category->name,['book/addcategory', 'id' =>$category->category_id] );
                            }
                          ?>
                                         
                 <div class="modal-footer">
                     <button type="button" class="btn btn-primary" id="add-book-btn">Add</button>
                      <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
              <?php Pjax::end(); ?>       
              </div>
           </div>
    </div> 
</div>
