<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
?>

<div class="modal fade" id="add-category-window">
            <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Add category</h4>
                  </div>
                  <div class="modal-body">   
                   <?php Pjax::begin(); ?>
                    <?php $categoryForm = ActiveForm::begin([
                                 'id' => 'add-category-form',
                                 'method' => 'post',
                                 'action' => ['category/add'],
                                 'options' => ['onsubmit' => 'return false','data-pjax' => true],
                                 'enableClientValidation' => true,
                                  ]); ?>

                      <div class="form-group">
                           <?= $categoryForm->field($addCategoryModel, 'name')->label("Name") ?>
                      </div>
                   
                 <div class="modal-footer">
                     <button type="submit" class="btn btn-primary">Add</button>
                      <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
                  <?php ActiveForm::end(); ?>  
              <?php Pjax::end(); ?>
              </div>
           </div>
    </div> 
</div>
